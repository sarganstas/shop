-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 05 2015 г., 14:15
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'notebooks'),
(2, 'tablets'),
(3, 'phones'),
(4, 'tv'),
(5, 'headphones'),
(7, 'photo');

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `good_name` varchar(255) NOT NULL,
  `configuration` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`id`, `id_category`, `good_name`, `configuration`, `price`, `desc`, `image`) VALUES
(1, 1, 'HP 255 G3', 'K3X67ES', 6299, 'Экран 15.6” (1366x768) HD LED, матовый / AMD Dual-Core E1-6010 (1.35 ГГц) / RAM 2 ГБ / HDD 500 ГБ / AMD Radeon R2 / без ОД / LAN / Bluetooth / Wi-Fi / веб-камера / Linux / 2.15 кг / черный\n', 'record_193336062.jpg'),
(2, 1, 'Lenovo G5045', '80E300HCUA', 5899, 'Экран 15.6" (1366x768) HD LED, глянцевый / AMD Dual-Core E1-6010 (1.35 ГГц) / RAM 2 ГБ / HDD 250 ГБ / AMD Radeon R2 / Без ОД / LAN / Wi-Fi / Bluetooth 4.0 / веб-камера / DOS / 2.5 кг / черный\n', 'record_185547752.jpg'),
(3, 2, 'Lenovo IdeaTab A5500 8 3G 16GB Red', '59413850', 3869, 'Экран 8" (1280 x 800) IPS емкостный, Multi-Touch / MediaTek 8121 (1.3 ГГц) / 1 ГБ оперативной памяти / 16 ГБ встроенной памяти + поддержка карт памяти microSD / 3G / Wi-Fi / Bluetooth 4.0 / основная камера 5.0 Мп, фронтальная - 2 Мп / GPS / Android 4.2 (Jelly Bean) / 360 г / краcный\n', 'record_113983485.jpg'),
(4, 2, 'Samsung Galaxy Tab 4 7.0 8GB 3G White', 'SM-T231NZWASEK', 4999, 'Экран 7" (1280x800) емкостный Multi-Touch / Qualcomm Snapdragon 400 (1.2 ГГц) / RAM 1.5 ГБ / 8 ГБ встроенной памяти + поддержка карт памяти microSD / Bluetooth 4.0 / Wi-Fi 802.11a/b/g/n / основная камера 3 Мп, фронтальная 1.3 Мп / 3G / GPS / ГЛОНАСС / Android 4.4.2 (KitKat) / 281 г / белый\n', 'record_91451409.jpg'),
(5, 2, 'LG G Pad 10.1 V700 Red', 'LGV700.ACISRD', 6555, 'Экран 10.1" (1280x800) IPS, емкостный, Multi-Touch / Qualcomm Snapdragon 400 (1.2 ГГц) / RAM 1 ГБ / 16 ГБ встроенной памяти + поддержка карт памяти microSD / Bluetooth 4.0 / Wi-Fi a/b/g/n / GPS / основная камера 5 Мп + фронтальная 1.3 Мп / ОС Android 4.4.2 (KitKat) / 523 г / красный', 'record_124352730.jpg'),
(6, 2, 'Asus Fonepad 7 3G 8GB Blue', 'FE170CG-6D020A', 3569, 'Экран 7" (1024x600) емкостный Multi-Touch / Intel Atom Z2520 (1.2 ГГц) / RAM 1 ГБ / 8 ГБ встроенной памяти + поддержка карт памяти microSD / 3G / Wi-Fi / Bluetooth 4.0 / основная камера 2 Мп, фронтальная - 0.3 Мп / A-GPS / ГЛОНАСС / Android 4.3 (Jelly Bean) / 290 г / синий\n', 'record_113985175.jpg'),
(7, 1, 'Acer Packard Bell ENTG71BMC83X', 'NX.C3UEU.100', 6199, 'Экран 15.6" (1366x768) HD LED, глянцевый / Intel Celeron N2840 (2.16 ГГц) / RAM 2 ГБ / HDD 500 ГБ / Intel HD Graphics / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / Linpus / 2.4 кг / черный\r\n', 'record_451769247.jpg'),
(8, 3, 'Lenovo A316i 3G Black', '', 1599, 'Экран 4" (800x480, сенсорный емкостный, Multi-Touch) / моноблок / MediaTek MT6572 1.3 ГГц / основная камера 2 Мп / Bluetooth 3.0 / Wi-Fi 802.11 b/g/n / 512 МБ оперативной памяти / 4 ГБ встроенной памяти + поддержка microSD / поддержка 2-х СИМ-карт / разъем 3.5 мм / 3G / GPS / ОС Android 4.2 (Jelly Bean) / 63.5 x 12.2 x 117 мм, 121 г / черный\r\n', 'record_111868485.jpg'),
(9, 3, 'Samsung Galaxy S5 G900H Gold', '', 11999, 'Экран 5.1" Super AMOLED (1920х1080, сенсорный емкостный, Multi-Touch) / моноблок / Exynos 5422 (Quad 1.9 ГГц + Quad 1.3 ГГц) / камера 16 Мп + фронтальная 2 Мп / Bluetooth 4.0 / Wi-Fi a/b/g/n/ac / 2 ГБ оперативной памяти / 16 ГБ встроенной памяти + поддержка microSD / пылевлагозащищённый / разъем 3.5 мм / 3G / GPS / OC Android 4.4.2 / 142 x 72.5 x 8.1 мм, 145 г / золотистый\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/filter/preset=smartfon/', 'record_347644472.jpg'),
(10, 3, 'LG Optimus L90 D405 Black', '', 3777, 'Экран 4.65" IPS (960x540) емкостный, Multi-Touch / моноблок / Qualcomm MSM8226 1.2 ГГц / камера 8 Мп + фронтальная 0.3 Мп / Bluetooth 4.0 / Wi-Fi b/g/n / 1 ГБ оперативной памяти / 8 ГБ встроенной памяти + поддержка microSD / разъем 3.5 мм / 4G / GPS / ОС Android 4.4.2 / 131.55 x 66.02 x 9.65 мм / черный\r\n', 'record_52578520.jpg'),
(47, 4, 'Телевизор LG 42LB690V', '', 18799, 'Диагональ экрана: 42"\r\nРазрешение: 1920x1080\r\nПоддержка Smart TV: Есть\r\nДиапазоны цифрового тюнера: DVB-S2, DVB-C, DVB-T2\r\nЧастота развертки панели: 200 Гц\r\nЧастота обновления: 700 Гц (MCI)\r\nWi-Fi: Да\r\nПодробнее: http://rozetka.com.ua/lg_42lb690v/p672134/', 'record_67457274.jpg'),
(49, 5, 'Koss Sporta Pro', '185597', 649, 'Тип наушников: Открытые\r\nИнтерфейс проводного подключения: 1 x mini-jack (разъем 3.5 мм)\r\nДиапазон частот наушников: 15 - 25000 Гц\r\nДлина шнура: 1.2 м\r\nВес: 62 г', 'record_413195307.jpg'),
(50, 7, 'Фотоаппарат Nikon Coolpix L830 Black', 'VNA600E1', 3999, '1/2.3" CMOS, 16 Мп / Зум: 34x (оптический), 4x (цифровой) / 59 МБ встроенной памяти + поддержка карт памяти SD/SDHC/SDXC / TFT дисплей 3" с наклоном / FullHD-видео / питание от четырех элементов типа АА / 111 x 75.8 x 91.2 мм, 508 г / черный', 'record_14380494.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `good_name` varchar(100) NOT NULL,
  `good_price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `name`, `city`, `phone`, `good_name`, `good_price`) VALUES
(32, 'taras', 'Kharkov', '12345678910', 'HP-255-G3', 6299),
(44, 'vasya', 'Kharkov', '0951839315', 'Samsung-Galaxy-Tab-4-7.0-8GB-3G-White', 4999),
(45, 'alex', 'Kiev', '0951839315', 'LG-Optimus-L90-D405-Black', 3777),
(46, 'Юра', 'Донецк', '0951839315', 'Lenovo-G5045', 5899),
(47, 'Иван', 'Москва', '0951839315', 'Lenovo-IdeaTab-A5500-8-3G-16GB-Red', 3869),
(48, 'Стас', 'Донецк', '0951839315', 'LG-Optimus-L90-D405-Black', 3777),
(49, 'Сарган Станислав', 'Харьков', '0951839315', 'Koss-Sporta-Pro', 649),
(50, 'Иван Иванов', 'Донецк', '0958799070', 'HP-255-G3', 6299);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roles` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `roles`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
