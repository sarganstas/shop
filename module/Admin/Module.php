<?php
namespace Admin;

use Admin\Model\Category;
use Admin\Model\CategoryTable;
use Admin\Model\Good;
use Admin\Model\GoodTable;
use Admin\Model\Order;
use Admin\Model\OrderTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/**
 * Module class
 *
 * Module class - class to work with the module admin
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'AdminModelCategoryTable' => function ($sm) {
                    $tableGateway = $sm->get('AdminTableGateway');
                    $table = new CategoryTable($tableGateway);
                    return $table;
                },
                'AdminTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Category());
                    return new TableGateway('category', $dbAdapter, null, $resultSetPrototype);
                },
                'AdminModelGoodTable' => function ($sm) {
                    $tableGateway = $sm->get('AdminGoodTableGateway');
                    $table = new GoodTable($tableGateway);
                    return $table;
                },
                'AdminGoodTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Good());
                    return new TableGateway('goods', $dbAdapter, null, $resultSetPrototype);
                },
                'AdminModelOrderTable' => function ($sm) {
                    $tableGateway = $sm->get('AdminOrderTableGateway');
                    $table = new OrderTable($tableGateway);
                    return $table;
                },
                'AdminOrderTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Order());
                    return new TableGateway('orders', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}