<?php
namespace Admin\Form;
use Zend\Form\Form;

/**
 * OrderForm class
 *
 * OrderForm - the form for page with order
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class OrderForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('order');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'City',
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Phone',
            ),
        ));
        $this->add(array(
            'name' => 'good_name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Good Name',
            ),
        ));
        $this->add(array(
            'name' => 'good_price',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Good Price',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Add',
                'id' => 'submitbutton',
                'class' => 'btn btn-success',
            ),
        ));
    }
}