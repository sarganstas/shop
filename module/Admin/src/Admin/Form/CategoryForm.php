<?php
namespace Admin\Form;

use Zend\Form\Form;

/**
 * CategoryForm class
 *
 * CategoryForm - the form for page with category
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class CategoryForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('category');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Add',
                'id' => 'submitbutton',
                'class' => 'btn btn-success',
            ),
        ));
    }
}