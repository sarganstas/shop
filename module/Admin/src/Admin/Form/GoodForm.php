<?php
namespace Admin\Form;
use Zend\Form\Form;

/**
 * GoodForm class
 *
 * GoodForm - the form for page with good
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class GoodForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('good');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'id_category',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'category',
            'options' => array(
                'label' => 'Choose Category',
            )
        ));
        $this->add(array(
            'name' => 'good_name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'configuration',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Configuration',
            ),
        ));
        $this->add(array(
            'name' => 'price',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Price',
            ),
        ));
        $this->add(array(
            'name' => 'desc',
            'attributes' => array(
                'type'  => 'textarea',
                'cols'  => '40',
                'rows'  => '10',
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'name' => 'image',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Image Upload',
                'attributes' => array(
                    'id' => 'image-file',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Add',
                'id' => 'submitbutton',
                'class' => 'btn btn-success',
            ),
        ));
    }
}