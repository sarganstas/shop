<?php

namespace Admin\Controller;

use Admin\Form\GoodForm;
use Admin\Form\OrderForm;
use Admin\Model\Good;
use Zend\File\Transfer\Adapter\Http;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\File\Size;
use Zend\View\Model\ViewModel;
use Admin\Model\Category;
use Admin\Form\CategoryForm;

/**
 * AdminController class
 *
 * AdminController class describes methods of work for the administration categories, goods, orders
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class AdminController extends AbstractActionController
{
    protected $categoryTable;
    protected $goodTable;
    protected $orderTable;

    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * View all categories
     *
     * @return array
     */
    public function categoriesAction()
    {
        return new ViewModel(array(
            'categories' => $this->getCategoryTable()->fetchAll(),
        ));
    }

    /**
     * View all goods
     *
     * @return array
     */
    public function goodsAction()
    {
        return new ViewModel(array(
            'goods' => $this->getGoodTable()->fetchAll(),
        ));
    }

    /**
     * View all orders
     *
     * @return array
     */
    public function ordersAction()
    {
        return new ViewModel(array(
            'orders' => $this->getOrderTable()->fetchAll(),
        ));
    }

    /**
     * Add category in database
     *
     * @return array
     */
    public function addCategoryAction()
    {
        $form = new CategoryForm();
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $category = new Category();
            $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $category->exchangeArray($form->getData());
                $this->getCategoryTable()->saveCategory($category);
                // Redirect to list of categories
                return $this->redirect()->toRoute('admin', array(
                    'action' => 'categories'
                ));
            }
        }
        return array('form' => $form);
    }

    /**
     * Add good in database
     *
     * @return array
     */
    public function addGoodAction()
    {
        $categories = $this->getCategoryTable()->fetchAll();
        foreach ($categories as $cat) :
            $catArr[$cat->id] = $cat->name;
        endforeach;
        $form = new GoodForm();
        $form->get('category')->setValueOptions($catArr);
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $good = new Good();
            $form->setInputFilter($good->getInputFilter());
            $nonFile = $request->getPost()->toArray();
            $file = $this->params()->fromFiles('image');
            $data = array_merge($nonFile, array('image'=> $file['name']));
            $form->setData($data);

            if ($form->isValid()) {
                $adapter = new Http();
                $adapter->setDestination($_SERVER["DOCUMENT_ROOT"].'/img/goods');
                $adapter->receive($file['name']);
                $good->exchangeArray($form->getData());

                $catId = $form->get('category')->getValue();
                $this->getGoodTable()->saveGood($good, $catId);
                return $this->redirect()->toRoute('admin', array(
                    'action' => 'goods'
                ));
            }
        }
        return array('form' => $form);
    }

    /**
     * Edit category in database
     *
     * @return array
     */
    public function editCategoryAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $category = $this->getCategoryTable()->getCategory($id);

        $form  = new CategoryForm();
        $form->bind($category);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getCategoryTable()->saveCategory($form->getData());
                // Redirect to list of categories
                return $this->redirect()->toRoute('admin', array(
                    'action' => 'categories'
                ));
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
     * Edit good in database
     *
     * @return array
     */
    public function editGoodAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $goodValues = $this->getGoodTable()->getGood($id);
        $nameCat = $this->getGoodTable()->getNameCat($id);
        $categories = $this->getCategoryTable()->fetchAll();
        foreach ($categories as $cat) :
            $catArr[$cat->id] = $cat->name;
        endforeach;

        $form  = new GoodForm();
        $form->bind($goodValues);
        $form->get('category')->setValueOptions($catArr)->setValue($nameCat['name']);
        $form->get('submit')->setAttribute('value', 'Edit');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $good = new Good();
            $form->setInputFilter($good->getInputFilter());
            $nonFile = $request->getPost()->toArray();
            $file = $goodValues->image;
            $data = array_merge($nonFile, array('image'=> $file));
            $form->setData($data);

            if ($form->isValid()) {
                $catId = $form->get('category')->getValue();
                $this->getGoodTable()->saveGood($form->getData(), $catId);
                // Redirect to list of goods
                return $this->redirect()->toRoute('admin', array(
                    'action' => 'goods'
                ));
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
     * Edit order in database
     *
     * @return array
     */
    public function editOrderAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $order = $this->getOrderTable()->getOrder($id);

        $form  = new OrderForm();
        $form->bind($order);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($order->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getOrderTable()->saveOrder($form->getData());
                // Redirect to list of categories
                return $this->redirect()->toRoute('admin', array(
                    'action' => 'orders'
                ));
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
     * Delete category in database
     *
     * @return array
     */
    public function deleteCategoryAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getCategoryTable()->deleteCategory($id);
            }
            // Redirect to list of categories
            return $this->redirect()->toRoute('admin', array(
                'action' => 'categories'
            ));
        }
        return array(
            'id'    => $id,
            'category' => $this->getCategoryTable()->getCategory($id)
        );
    }

    /**
     * Delete good in database
     *
     * @return array
     */
    public function deleteGoodAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getGoodTable()->deleteGood($id);
            }
            // Redirect to list of categories
            return $this->redirect()->toRoute('admin', array(
                'action' => 'goods'
            ));
        }
        return array(
            'id'    => $id,
            'good' => $this->getGoodTable()->getGood($id)
        );
    }

    /**
     * Delete order in database
     *
     * @return array
     */
    public function deleteOrderAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getOrderTable()->deleteOrder($id);
            }
            // Redirect to list of categories
            return $this->redirect()->toRoute('admin', array(
                'action' => 'orders'
            ));
        }
        return array(
            'id'    => $id,
            'order' => $this->getOrderTable()->getOrder($id)
        );
    }

    /**
     * get category table
     *
     * @return array
     */
    public function getCategoryTable()
    {
        if (!$this->categoryTable) {
            $sm = $this->getServiceLocator();
            $this->categoryTable = $sm->get('Admin\Model\CategoryTable');
        }
        return $this->categoryTable;
    }

    /**
     * get good table
     *
     * @return array
     */
    public function getGoodTable()
    {
        if (!$this->goodTable) {
            $sm = $this->getServiceLocator();
            $this->goodTable = $sm->get('Admin\Model\GoodTable');
        }
        return $this->goodTable;
    }

    /**
     * get order table
     *
     * @return array
     */
    public function getOrderTable()
    {
        if (!$this->orderTable) {
            $sm = $this->getServiceLocator();
            $this->orderTable = $sm->get('Admin\Model\OrderTable');
        }
        return $this->orderTable;
    }
}