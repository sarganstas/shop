<?php
namespace Admin\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

/**
 * CategoryTable class
 *
 * CategoryTable class contains methods for working with a table category
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class CategoryTable
{
    protected $tableGateway;

    /**
     * gets adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all records from category table
     *
     * @return array
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * get category by id
     *
     * @param integer $id
     * @return array
     */
    public function getCategory($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * save category data
     *
     * @param array $Category post-data
     */
    public function saveCategory(Category $Category)
    {
        $data = array(
            'name'  => $Category->name,
        );
        $id = (int)$Category->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCategory($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    /**
     * delete category by id
     *
     * @param integer $id
     * @return array
     */
    public function deleteCategory($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}