<?php
namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

/**
 * Good class
 *
 * Good class - class to work with the model good
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class Good
{
    public $id;
    public $id_category;
    public $category;
    public $good_name;
    public $configuration;
    public $price;
    public $desc;
    public $image;

    /**
     * Copies the data that came in the form of an array in the properties of an entity
     *
     * @return array
     */
    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->id_category = (isset($data['id_category'])) ? $data['id_category'] : null;
        $this->category = (isset($data['category'])) ? $data['category'] : null;
        $this->good_name = (isset($data['good_name'])) ? $data['good_name'] : null;
        $this->configuration = (isset($data['configuration'])) ? $data['configuration'] : null;
        $this->price = (isset($data['price'])) ? $data['price'] : null;
        $this->desc = (isset($data['desc'])) ? $data['desc'] : null;
        $this->image = (isset($data['image'])) ? $data['image'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new Exception("Not used");
    }

    /**
     * adds filters for inputs
     *
     * @return array
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id_category',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'good_name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'configuration',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'price',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'Digits',
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'desc',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}