<?php
namespace Admin\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

/**
 * OrderTable class
 *
 * OrderTable class contains methods for working with a table order
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class OrderTable
{
    protected $tableGateway;

    /**
     * gets db adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all records from order table
     *
     * @return array
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * get order by id
     *
     * @param integer $id
     * @return array
     */
    public function getOrder($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * save order data
     *
     * @param array $Order post-data
     */
    public function saveOrder(Order $Order)
    {
        $data = array(
            'name' => $Order->name,
            'city' => $Order->city,
            'phone' => $Order->phone,
            'good_name'  => $Order->good_name,
            'good_price'  => $Order->good_price,
        );

        $id = (int)$Order->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getOrder($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    /**
     * delete order by id
     *
     * @param integer $id
     * @return array
     */
    public function deleteOrder($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}