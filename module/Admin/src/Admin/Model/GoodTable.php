<?php
namespace Admin\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

/**
 * GoodTable class
 *
 * GoodTable class contains methods for working with a good basket
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class GoodTable
{
    protected $tableGateway;

    /**
     * gets adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all records from goods table
     *
     * @return array
     */
    public function fetchAll()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->join('category', 'id_category = category.id', array('name'))
                ->order('id DESC');
        $resultSet = new ResultSet();
        return $resultSet->initialize($this->tableGateway->getSql()->prepareStatementForSqlObject($select)->execute());
    }

    /**
     * get good data by id
     *
     * @param integer $id
     * @return array
     */
    public function getGood($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * get name category by id
     *
     * @param integer $id
     * @return array
     */
    public function getNameCat($id)
    {
        $id  = (int) $id;
        $select = $this->tableGateway->getSql()->select();
        $select->join('category', 'id_category = category.id', array('name'))
               ->where(array('goods.id' => $id));
        $resultSet = new ResultSet();
        $resultSet->initialize($this->tableGateway->getSql()->prepareStatementForSqlObject($select)->execute());
        return $row = $resultSet->current();
    }

    /**
     * save good data
     *
     * @param array $Good post-data
     * @param integer $catId
     */
    public function saveGood(Good $Good, $catId)
    {
        $data = array(
            'id_category' => $catId,
            'good_name' => $Good->good_name,
            'configuration' => $Good->configuration,
            'price' => $Good->price,
            'desc'  => $Good->desc,
            'image'  => $Good->image,
        );

        $id = (int)$Good->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getGood($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    /**
     * delete good by id
     *
     * @param integer $id
     * @return array
     */
    public function deleteGood($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}