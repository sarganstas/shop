<?php
/**
 * @return string Resources available to the guest / admin.
 */

return array(
    'guest' => array(
        'home',
    ),
    'admin' => array(
        'admin',
        'home',
    ),
);