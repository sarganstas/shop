<?php

namespace Application\Controller;

use Application\Model\CategoryTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * IndexController class
 *
 * IndexController class which view all categories on index page
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class IndexController extends AbstractActionController
{

    /**
     * View all categories on index page
     *
     * @return array
     */
    public function indexAction()
    {
        $catModel = new CategoryTable($this->serviceLocator->get('ZendDbAdapterAdapter'));
        return new ViewModel(array(
            'categories' => $catModel->fetchAll(),
        ));
    }
}
