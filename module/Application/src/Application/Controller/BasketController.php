<?php

namespace Application\Controller;

use Application\Form\BasketForm;
use Application\Model\Basket;
use Application\Model\BasketTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * BasketController class
 *
 * BasketController class to work with basket
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class BasketController extends AbstractActionController
{

    /**
     * Show the form of basket to confirm the order
     *
     * @return array
     */
    public function indexAction()
    {
        $form = new BasketForm();
        $name = $this->params()->fromRoute('name', 0);
        $price = $this->params()->fromRoute('price', 0);
        if (!$name || !$price) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();

        if ($request->isPost()) {
            $basket = new Basket();
            $form->setInputFilter($basket->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $basketModel = new BasketTable($this->serviceLocator->get('ZendDbAdapterAdapter'));
                $basketModel->saveOrder($request->getPost(), $name, $price);
                $this->flashMessenger()->addMessage('Спасибо! Мы свяжемся с вами.');
                return $this->redirect()->toRoute('success');
            }
        }
        return new ViewModel(array(
            'good_name' => $name,
            'good_price' => $price,
            'form' => $form,
        ));
    }

    /**
     * Shows the user a message about the successful ordering
     *
     * @return array
     */
    public function successAction()
    {
        return new ViewModel(array(
            'flashMessages' => $this->flashMessenger()->getMessages()
        ));
    }
}