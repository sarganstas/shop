<?php

namespace Application\Controller;

use Application\Model\GoodTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * CategoryController class
 *
 * CategoryController class which shows the products in category
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class CategoryController extends AbstractActionController
{

    public function indexAction()
    {
    }

    /**
     * Show the products in category
     *
     * @return array
     */
    public function viewAction()
    {
        $name = $this->params()->fromRoute('name', 0);
        if (!$name) {
            return $this->redirect()->toRoute('home');
        }
        $goodModel = new GoodTable($this->serviceLocator->get('ZendDbAdapterAdapter'));
        return new ViewModel(array(
            'goods' => $goodModel->getGoodsFromCat($name),
        ));
    }

}
