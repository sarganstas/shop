<?php

namespace Application\Controller;

use Application\Model\GoodTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * GoodController class
 *
 * GoodController class which view details of good
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class GoodController extends AbstractActionController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * View details of good
     *
     * @return array
     */
    public function viewAction()
    {
        $name = $this->params()->fromRoute('name', 0);
        if (!$name) {
            return $this->redirect()->toRoute('home');
        }
        $goodModel = new GoodTable($this->serviceLocator->get('ZendDbAdapterAdapter'));
        return new ViewModel(array(
            'good' => $goodModel->getGood($name),
        ));
    }
}
