<?php
namespace Application\Model;

use Zend\InputFilter\InputFilterInterface;

/**
 * Good class
 *
 * Good class - class to work with the model good
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class Good
{
    public $id;
    public $id_category;
    public $name_good;
    public $price;
    public $desc;

    /**
     * Copies the data that came in the form of an array in the properties of an entity
     *
     * @return array
     */
    public function exchangeArray($data)
    {
        $this->id          = (isset($data['id'])) ? $data['id'] : null;
        $this->id_category = (isset($data['id_category'])) ? $data['id_category'] : null;
        $this->name        = (isset($data['name_good'])) ? $data['name_good'] : null;
        $this->price       = (isset($data['price'])) ? $data['price'] : null;
        $this->desc        = (isset($data['desc'])) ? $data['desc'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new Exception("Not used");
    }

}