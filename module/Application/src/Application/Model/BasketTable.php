<?php
namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

/**
 * BasketTable class
 *
 * BasketTable class contains methods for working with a table basket
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class BasketTable
{
    protected $adapter;

    /**
     * gets adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct($adapter)
    {
        if ($adapter) {
            $this->sql = new Sql($adapter);
            $this->adapter = $adapter;
        }
    }

    /**
     * save order data
     *
     * @param array $post post-data
     * @param string $name value name from Get request
     * @param string $price value price from Get request
     */
    public function saveOrder($post, $name, $price)
    {
        $insert = $this->sql->insert('orders');
        $newData = array(
            'name' => $post->name,
            'city' => $post->city,
            'phone' => $post->phone,
            'good_name' => $name,
            'good_price' => $price,
        );
        $insert->values($newData);
        $this->sql->prepareStatementForSqlObject($insert)->execute();
    }

}