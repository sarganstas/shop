<?php
namespace Application\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

/**
 * GoodTable class
 *
 * GoodTable class contains methods for working with a good basket
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class GoodTable
{

    /**
     * gets adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct($adapter)
    {
        if ($adapter) {
            $this->sql = new Sql($adapter);
        }
    }

    /**
     * get goods data from category
     *
     * @param string $name value name from Get request
     * @return array
     */
    public function getGoodsFromCat($name)
    {
        $select = $this->sql->select();
        $select->from(array('g' => 'goods'))
            ->join(array('c' => 'category'), 'g.id_category = c.id', array('*'), $select::JOIN_LEFT)
            ->where(array('c.name' => $name));
        $resultSet = new ResultSet;
        return $resultSet->initialize($this->sql->prepareStatementForSqlObject($select)->execute());
    }

    /**
     * get good data by name
     *
     * @param string $name value name from Get request
     * @return array
     */
    public function getGood($name)
    {
        $good_name = str_replace ('-', ' ', $name);
        $select = $this->sql->select();
        $select->from('goods')
            ->where(array('good_name' => $good_name));
        $resultSet = new ResultSet;
        return $resultSet->initialize($this->sql->prepareStatementForSqlObject($select)->execute());
    }

}