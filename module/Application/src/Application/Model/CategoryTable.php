<?php
namespace Application\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

/**
 * CategoryTable class
 *
 * CategoryTable class contains methods for working with a table category
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class CategoryTable
{
    protected $tableGateway;

    /**
     * gets db adapter in construct
     *
     * @param array $adapter Zend\Db\Adapter\Adapter Object
     */
    public function __construct($adapter)
    {
        if ($adapter) {
            $this->sql = new Sql($adapter);
        }
    }

    /**
     * fetch all records from category table
     *
     * @return array
     */
    public function fetchAll()
    {
        $select = $this->sql->select();
        $select->from('category');
        $resultSet = new ResultSet();
        return $resultSet->initialize($this->sql->prepareStatementForSqlObject($select)->execute());
    }

    /**
     * get category by id
     *
     * @param integer $id
     * @return array
     */
    public function getCategory($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
}