<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * BasketForm class
 *
 * BasketForm - the form for page with basket
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class BasketForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('basket');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Имя и фамилия',
            ),
        ));
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Город',
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'type' => 'Application\Form\Element\Phone',
            'options' => array(
                'label' => 'Телефон',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Заказ подтверждаю',
                'id' => 'submitbutton',
                'class' => 'btn btn-success',
            ),
        ));
    }
}