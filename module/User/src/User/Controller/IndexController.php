<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Authentication\Result;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

use User\Model\User;
use User\Form\LoginForm;

/**
 * IndexController class
 *
 * IndexController class describes methods of work for the user
 *
 * @author Sargan Stas <sargan.stas@mail.ru>
 * @version 1.0
 */

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * method for user login
     *
     * @return array
     */
    public function loginAction()
    {
        $user = $this->identity();
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
        $messages = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $authFormFilters = new User();
            $form->setInputFilter($authFormFilters->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                $sm = $this->getServiceLocator();
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $authAdapter = new AuthAdapter($dbAdapter,
                                            'users', // there is a method setTableName to do the same
                                            'name', // there is a method setIdentityColumn to do the same
                                            'password', // there is a method setCredentialColumn to do the same
                                            "MD5(?)" // setCredentialTreatment(parametrized string) 'MD5(?)'

                );
                $authAdapter
                    ->setIdentity($data['name'])
                    ->setCredential($data['password']);

                $auth = new AuthenticationService();
                $result = $auth->authenticate($authAdapter);

                switch($result->getCode()) {
                    case Result::FAILURE_IDENTITY_NOT_FOUND:
                        // do stuff for nonexistent identity
                        break;
                    case Result::FAILURE_CREDENTIAL_INVALID:
                        // do stuff for invalid credential
                        break;
                    case Result::SUCCESS:
                        $storage = $auth->getStorage();
                        $storage->write($authAdapter->getResultRowObject(
                            null,
                            'password'
                        ));
                        $time = 1209600; //14 days
                        if($data['rememberme']) {
                            $sessionManager = new \Zend\Session\SessionManager();
                            $sessionManager->rememberMe($time);
                        }
                        break;
                    default:
                        // do stuff for other failure
                        break;
                }
                foreach($result->getMessages() as $messages) {
                    $messages .= "\n";
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'messages' => $messages,
        ));
    }

    /**
     * method for user logout
     *
     * @return array
     */
    public function logoutAction()
    {
        $auth = new AuthenticationService();
        if($auth->hasIdentity()){
            $identity = $auth->getIdentity();
        }
        $auth->clearIdentity();
        $sessionManager = new \Zend\Session\SessionManager();
        $sessionManager->forgetMe();
        return $this->redirect()->toRoute('user/default', array('controller' => 'index', 'action' => 'login'));
    }
}